
const bull = require('bull');

const redis = { host: 'LOCALHOST', port: 6379 };

const opts = { redis: { host: redis.host, port: redis.port } };

const queueCreate = bull("curso:create", opts);
const queueDelete = bull("curso:delte", opts);
const queueUpdate = bull("curso:update", opts);
const queueFindOne = bull("curso:findone", opts);
const queueView = bull("curso:view", opts);

async function Create({ age, color, name }) {
   try {

      const job = await queueCreate.add({ age, color, name })

      const { statusCode, data, message } = await job.finished()

      return { statusCode, data, message};


   } catch (error) {
      console.log(error)
   }

};

async function Delete({ id }) {
   try {

      const job = await queueDelete.add({ id })

      const { statusCode, data, message } = await job.finished()

      return { statusCode, data, message};



   } catch (error) {
      console.log(error)
   }

};

async function FindOne({ id }) {
   try {

      const job = await queueFindOne.add({ id })

      const { statusCode, data, message } = await job.finished()
      return { statusCode, data, message};


   } catch (error) {
      console.log(error)
   }

};

async function Update({ age, color, name, id }) {
   try {

      const job = await queueUpdate.add({ age, color, name, id })

      const { statusCode, data, message } = await job.finished()
      return { statusCode, data, message};

   } catch (error) {
      console.log(error)
   }

};

async function View() {
   try {


      const job = await queueView.add({})

      const { statusCode, data, message } = await job.finished()


      return { statusCode, data, message};


   } catch (error) {
      console.log(error)
   }

};





module.exports = {Create, Delete, Update, FindOne, View}