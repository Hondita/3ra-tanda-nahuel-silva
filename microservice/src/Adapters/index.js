const bull = require('bull');

const { redis } = require('../settings');

const opts = { redis: { host: redis.host, port: redis.port } };

const queueCreate = bull("curso:create", opts);
const queueDelete = bull("curso:delte", opts);
const queueUpdate = bull("curso:update", opts);
const queueFindOne = bull("curso:findone", opts);
const queueView = bull("curso:view", opts);

module.exports = { queueView, queueCreate, queueDelete, queueUpdate, queueFindOne };