const Services = require('../Services')
const {internalError} = require('../settings')
const {queueView, queueCreate, queueDelete, queueFindOne, queueUpdate} = require('./index')

async function View (job, done) {

    try {

        const { } = job.data;

        console.log(job.id)


        let {statusCode, data, message} = await Services.View({ });

      //  console.log({statusCode, data, message})
        
        done(null, {statusCode, data, message}); 
    
    } catch (error) {
        console.log({step: 'adapter queueView', error: error.toString()})

        done(null, {statusCode: 500, message: internalError}); 
        error
    }



};

async function Create (job, done) {

    try {

        const { age, color, name } = job.data;


        let {statusCode, data, message} = await Services.Create({age, color, name });

        //console.log({statusCode, data, message})
        
        done(null, {statusCode, data, message}); 
    
    } catch (error) {
        console.log({step: 'adapter queueCreate', error: error.toString()})

        done(null, {statusCode: 500, message: internalError}); 


    }



};

async function Delete(job, done) {

    try {

        const { id } = job.data;


        let {statusCode, data, message} = await Services.Delete({ id });

        //console.log({statusCode, data, message})
        
        done(null, {statusCode, data, message}); 
    
    } catch (error) {
        console.log({step: 'adapter queueDelete', error: error.toString()})

        done(null, {statusCode: 500, message: internalError}); 


    }



};

async function FindOne(job, done) {

    try {

        const { id } = job.data;

      

        let {statusCode, data, message} = await Services.FindOne({ id });

       // console.log({statusCode, data, message})
        
        done(null, {statusCode, data, message}); 
    
    } catch (error) {
        console.log({step: 'adapter queueFindOne', error: error.toString()})

        done(null, {statusCode: 500, message: internalError}); 


    }



};

async function Update(job, done) {

    try {

        const { age, color, name , id } = job.data;


        let {statusCode, data, message} = await Services.Update({ age, color, name , id});

       // console.log({statusCode, data, message})
        
        done(null, {statusCode, data, message}); 
    
    } catch (error) {
        console.log({step: 'adapter queueUpdate', error: error.toString()})

        done(null, {statusCode: 500, message: internalError}); 


    }



};

async function run () {
    try {

        console.log("Vamos a inicializar worker")
        queueView.process(View);

        queueCreate.process(Create);

        queueDelete.process(Delete);
        queueUpdate.process(Update);
        queueFindOne.process(FindOne);


        

    } catch (error) {
        console.log(error)
    }

}

module.exports = {
    View, Create, Delete, FindOne, Update, run
}